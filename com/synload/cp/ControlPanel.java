package com.synload.cp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.JclObjectFactory;

import com.synload.cp.dashboard.Dashboard;
import com.synload.cp.dashboard.DashboardGroup;
import com.synload.cp.dashboard.DashboardItem;
import com.synload.cp.handlers.HTTPHandler;
import com.synload.cp.handlers.Request;
import com.synload.cp.js.Javascript;
import com.synload.cp.menu.MenuItem;
import com.synload.cp.ws.WebsocketHandler;
import com.synload.eventsystem.Addon;

public class ControlPanel{
	public ControlPanel(){}
	public static HashMap<String,HashMap<String,Object>> htmlFiles = new HashMap<String,HashMap<String,Object>>();
	public static List<Session> users = new ArrayList<Session>();
	public static Map<String,DashboardGroup> dashboardGroups = new HashMap<String,DashboardGroup>();
	public static EntityManagerFactory emf = null;
	public static List<Object> plugins = new ArrayList<Object>();
	public static List<String> bannedIPs = new ArrayList<String>();
	public static int totalFailures = 10;
	public static Properties prop = new Properties();
	public static Map<String,List<Long>> failedAttempts = new HashMap<String,List<Long>>();
	public static List<Javascript> javascripts = new ArrayList<Javascript>();
	public static EntityManager em = null;
	public static String authKey = "s9V0l3v1GsrE2j50VrUp1Elp1jY4Xh97bNkuHBnOVCL28I"+
			"TyH17u5TRD25UDsRrb2Bny61y1XXv0zZSWq4O9gARzO881amS3lAgy";
	public static List<MenuItem> menus = new ArrayList<MenuItem>();
	public static MenuItem parentMenuGames = null;
	public static MenuItem parentMenuUsers = null;
	public static void main(String[] args) {
		try {
			if((new File("config.ini")).exists()){
				prop.load(new FileInputStream("config.ini"));
				authKey = prop.getProperty("authKey");
			}else{
				prop.setProperty("authKey", authKey);
				prop.setProperty("gameMenu", "true");
				prop.store(new FileOutputStream("config.ini"), null);
			}
			Map<String, String> properties = new HashMap<String, String>();
			System.out.println("[DEFAULTS] Loading defaults");
			ControlPanel.buildDashboard();
			ControlPanel.buildMenu();
			ControlPanel.buildJavascript();
			System.out.println("[DEFAULTS] Fully loaded defaults");
			
			System.out.println("[MODULE] Loading modules");
	        JarClassLoader jcl = new JarClassLoader();
	        String path = "modules/";
	        String files;
	        File folder = new File(path);
	        if(!folder.exists()){
	            folder.mkdir();
	        }
	        File[] listOfFiles = folder.listFiles(); 
	        for (int i = 0; i < listOfFiles.length; i++){
	            if (listOfFiles[i].isFile()){
	                files = listOfFiles[i].getName();
	                if (files.endsWith(".jar")){
	                    jcl = new JarClassLoader();
	                    String[] filedata = files.split("\\.");
	                    jcl.add("modules/"+files);
	                    JclObjectFactory factory = JclObjectFactory.getInstance();
	                    Object obj = factory.create(jcl, "org.gcp."+filedata[0]+".Main");
	                    plugins.add(obj);
	                    ((Addon)obj).init();
	                    System.out.println("[MODULE] Loaded module "+filedata[0]);
	                }
	            }
	        }
	        System.out.println("[MODULE] Modules fully loaded");
			int port = 80;
			if(args.length>=1){
				port = Integer.valueOf(args[0]);
			}
			
			properties.put("javax.persistence.jdbc.user", "admin");
			properties.put("javax.persistence.jdbc.password", "admin");
			emf = Persistence.createEntityManagerFactory("objectdb://localhost:6136/gcp.odb", properties);
			em = ControlPanel.emf.createEntityManager();
			if(em.isOpen()){
				System.out.println("[OBJECTDB] connected");
			}
			
			Server server = new Server(port);
			HandlerCollection handlerCollection = new HandlerCollection();
			handlerCollection.addHandler(new HTTPHandler());
			handlerCollection.addHandler(new WebsocketHandler());
			server.setHandler(handlerCollection);
			
			System.out.println("[HTTP/WEBSOCKET] Started server on port "+port);
			server.start();
			server.join();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ControlPanel.emf.close();
	}
	public static void buildJavascript(){
		Javascript jsUser = new Javascript();
		jsUser.addCallBack("user.msg", "message");
		jsUser.addCallBack("user.load", "init");
		jsUser.addCallBack("user.status", "loggedin");
		jsUser.addCallBack("user.dashboard","dashboard");
		jsUser.setScript("/js/user.js");
		ControlPanel.addJavascriptFile(jsUser);
		Javascript jsPage = new Javascript();
		jsPage.setScript("/js/page.js");
		ControlPanel.addJavascriptFile(jsPage);
		Javascript jsForm = new Javascript();
		jsForm.setScript("/js/form.js");
		ControlPanel.addJavascriptFile(jsForm);
	}
	public static void buildDashboard(){
		/*GameControlPanel.createDashboardItem(
				"Order Game Server", 
				"Order a game server for this account!", 
				"http://synload.com/media/apwhite/gamepad@2x.png", 
				"blue", "addGame", "get","Games","g");*/
		/*
		 * Second Dashboard Item
		 */
		ControlPanel.createDashboardItem(
				"Account Settings", 
				"Change settings on this account!", 
				"http://synload.com/media/apwhite/settings@2x.png", 
				"orange", "userSettings", "get","Account Profile","acp","account");
	}
	public static void buildMenu(){
		if(prop.getProperty("gameMenu").toString().equalsIgnoreCase("true")){
			ControlPanel.parentMenuGames = ControlPanel.createMenu("Games", "games", "", "", null, "game");
			/*
			 * GameControlPanel.createMenu("Order Game Server", "addgameserver", "addGame", "get", GameControlPanel.parentMenuGames);
			 */
			ControlPanel.menus.add(ControlPanel.parentMenuGames);
		}
		/*
		 * Second Menu Item, User
		 */
		ControlPanel.parentMenuUsers = ControlPanel.createMenu("Account", "account", "", "", null, "account");
		ControlPanel.createMenu("Edit Profile", "usersettings", "userSettings", "get", ControlPanel.parentMenuUsers,"account");
		ControlPanel.menus.add(ControlPanel.parentMenuUsers);
	}
	public static DashboardItem createDashboardItem(String name, String description, String image, String color, String page, String request, String groupName,String gUnique, String flag){
		DashboardItem d = new DashboardItem();
			Request r = new Request();
			r.setPage(page);
			r.setRequest(request);
		d.setRequest(r);
		d.setColor(color);
		d.setName(name);
		d.setFlag(flag);
		d.setDescription(description);
		d.setImage(image);
		if(ControlPanel.dashboardGroups.containsKey(gUnique)){
			ControlPanel.dashboardGroups.get(gUnique).addDashboards(d);
		}else{
			DashboardGroup dashG = new DashboardGroup(); 
			dashG.addDashboards(d);
			dashG.setName(groupName);
			ControlPanel.dashboardGroups.put(gUnique,dashG);
			Dashboard.addItems(dashG);
		}
		return d;
	}
	public static MenuItem createMenu(String name, String uname, String page, String request, MenuItem parent, String flag){
		MenuItem m = new MenuItem();
		m.setName(name);
		m.setUname(uname);
		m.setFlag(flag);
			Request r = new Request();
			r.setPage(page);
			r.setRequest(request);
		m.setRequest(r);
		if(parent!=null){
			parent.addMenus(m);
		}
		return m;
	}
	public static void addJavascriptFile(Javascript js){
		ControlPanel.javascripts.add(js);
	}
}