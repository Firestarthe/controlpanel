package com.synload.cp.elements;

import java.util.HashMap;
import java.util.Map;

import com.synload.cp.handlers.Response;

public class LoginBox extends Response{
	public LoginBox(){
		this.setTemplate(this.getTemplate("./elements/login/box.html"));
		this.setAction("alone");
		this.setParent("#content");
		this.setParentTemplate("full");
    	Map<String,String> tmp = new HashMap<String,String>();
    	tmp.put("username", "username");
    	tmp.put("password", "password");
    	tmp.put("input", "#username");
		this.setData(tmp);
		this.addJavascript(this.getFileData("./elements/js/focus_input.js"));
	}
}