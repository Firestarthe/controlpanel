package com.synload.cp.elements;

import java.util.List;

import com.synload.cp.ControlPanel;
import com.synload.cp.handlers.Response;
import com.synload.cp.js.Javascript;

public class JavascriptIncludes extends Response{
	public List<Javascript> javascripts = ControlPanel.javascripts;
	public String js_template = this.getFileData("./elements/js/include_script.js");
	public int jscount = ControlPanel.javascripts.size();
}