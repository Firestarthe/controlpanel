package com.synload.cp.elements;

import com.synload.cp.handlers.Response;

public class RegisterBox extends Response{
	public RegisterBox(){
		this.setTemplate(this.getTemplate("./elements/register/box.html"));
		this.setAction("alone");
		this.setParent("#content");
		this.setParentTemplate("full");
	}
}