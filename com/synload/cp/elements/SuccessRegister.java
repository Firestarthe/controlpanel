package com.synload.cp.elements;

import java.util.HashMap;
import java.util.Map;

import com.synload.cp.handlers.Response;

public class SuccessRegister extends Response{
	public SuccessRegister(){
		this.setTemplate(this.getTemplate("./elements/register/success.html"));
		this.setAction("wait");
		Map<String,String> redirect = new HashMap<String,String>();
		redirect.put("request", "get");
		redirect.put("page", "login");
		this.setRedirect( redirect );
		this.setSleep(2000);
		this.setParent("#content");
		this.setParentTemplate("full");
	}
}