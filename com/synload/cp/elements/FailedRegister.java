package com.synload.cp.elements;

import java.util.HashMap;
import java.util.Map;

import com.synload.cp.handlers.Response;

public class FailedRegister extends Response{
	public FailedRegister(){
		this.setTemplate(this.getTemplate("./elements/register/fail.html"));
		this.setAction("wait");
		Map<String,String> redirect = new HashMap<String,String>();
		redirect.put("request", "get");
		redirect.put("page", "register");
		this.setRedirect( redirect );
		this.setSleep(2000);
		this.setParent("#content");
		this.setParentTemplate("full");
	}
}