package com.synload.cp.elements;



import com.synload.cp.handlers.Response;

public class SuccessLogin extends Response{
	public SuccessLogin(){
		this.setTemplate(this.getTemplate("./elements/login/success.html"));
		this.setAction("wait");
		//Map<String,String> redirect = new HashMap<String,String>();
		//redirect.put("request", "get");
		//redirect.put("page", "dashboardItems");
		//this.setRedirect( redirect );
		this.setCallEvent("loggedin");
		this.setSleep(1000);
		this.setParent("#content");
		this.setParentTemplate("full");
	}
}