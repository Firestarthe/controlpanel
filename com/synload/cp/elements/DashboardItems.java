package com.synload.cp.elements;

import java.util.List;

import com.synload.cp.dashboard.Dashboard;
import com.synload.cp.dashboard.DashboardGroup;
import com.synload.cp.handlers.Response;

public class DashboardItems extends Response{
	public List<DashboardGroup> groups = null;
	public DashboardItems(){
		this.groups = Dashboard.groups;
		this.setTemplate(this.getTemplate("./elements/dashboard/itemlist.html"));
		this.addJavascript(this.getFileData("./elements/js/menu_change.js"));
		this.data.put("uname", "home");
		this.setParent(".items[page='dashboard']");
		this.setAction("alone");
		this.setParentTemplate("dashboardPage");
	}
}
