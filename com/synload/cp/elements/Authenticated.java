package com.synload.cp.elements;

import com.synload.cp.handlers.Response;

public class Authenticated extends Response{
	public Authenticated(){
		this.setCallEvent("authenticated");
		this.setAction("event");
	}
}