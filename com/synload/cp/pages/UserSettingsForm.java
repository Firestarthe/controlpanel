package com.synload.cp.pages;

import com.synload.cp.forms.Description;
import com.synload.cp.forms.Form;
import com.synload.cp.forms.Text;
import com.synload.cp.handlers.Request;
import com.synload.cp.ws.WSHandler;

public class UserSettingsForm {
	public static Form get(WSHandler user){
		Form f = new Form();
		f.setHeader("Edit user settings");
			Request r = new Request();
			r.setRequest("action");
			r.setPage("userSettings");
		f.setParent(".items[page='dashboard']");
		f.setParentTemplate("dashboardPage");
			Text t = new Text();
			t.setEnabled(true);
			t.setLabel("Email");
			t.setIdentifier("email");
			t.setValue(user.getUser().getEmail());
		f.addFormItem(t);
			Text fn = new Text();
			fn.setEnabled(true);
			fn.setLabel("First Name");
			fn.setIdentifier("firstname");
			fn.setValue(user.getUser().getFirstName());
		f.addFormItem(fn);
			Text ln = new Text();
			ln.setEnabled(true);
			ln.setLabel("Last Name");
			ln.setIdentifier("lastname");
			ln.setValue(user.getUser().getLastName());
		f.addFormItem(ln);
			Description d = new Description();
			d.setDescription("The options below are completely optional and are not required.");
		f.addFormItem(d);
			Text co = new Text();
			co.setEnabled(true);
			co.setLabel("Country");
			co.setIdentifier("country");
			co.setValue(user.getUser().getCountry());
		f.addFormItem(co);
			Text ci = new Text();
			ci.setEnabled(true);
			ci.setLabel("City");
			ci.setIdentifier("city");
			ci.setValue(user.getUser().getCity());
		f.addFormItem(ci);
		f.addJavascript(f.getFileData("./elements/js/menu_change.js"));
		f.setRequest(r);
		f.data.put("uname", "usersettings");
		return f;
	}
}
