package com.synload.cp.pages;

import com.synload.cp.handlers.Response;

public class FullPage extends Response{
	public FullPage(){
		this.setTemplate(this.getTemplate("./pages/full.html"));
		this.setAction("alone");
		this.setParent("#inner[element='body']");
		this.setParentTemplate("full");
	}
}