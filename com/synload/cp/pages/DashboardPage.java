package com.synload.cp.pages;

import java.util.ArrayList;
import java.util.List;

import com.synload.cp.ControlPanel;
import com.synload.cp.handlers.Response;
import com.synload.cp.menu.MenuItem;

public class DashboardPage extends Response{
	public List<MenuItem> menus = new ArrayList<MenuItem>();
	public DashboardPage(){
		this.menus = ControlPanel.menus;
		this.setTemplate(this.getTemplate("./pages/dashboard.html"));
		this.setAction("alone");
		this.setParent("#content");
		this.setParentTemplate("full");
		this.setCallEvent("dashboard");
	}
}