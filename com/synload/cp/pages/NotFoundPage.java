package com.synload.cp.pages;


import com.synload.cp.handlers.Response;

public class NotFoundPage extends Response{
	public NotFoundPage(){
		this.setTemplate(this.getTemplate("./elements/404.html"));
		this.setAction("alone");
		this.setParent(".items[page='dashboard']");
		this.setParentTemplate("dashboardPage");
		this.setCallEvent("dashboard");
	}
}
