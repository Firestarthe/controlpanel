package com.synload.cp.models;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.synload.cp.ControlPanel;

@Entity
@JsonTypeInfo(
	use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "class"
)
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT c FROM User c"),
    @NamedQuery(name="User.findByName", query="SELECT c FROM User c WHERE c.username = :name"),
}) 
public class User{
	@Id @GeneratedValue long _id;
	private String username, email, firstName, lastName, city, country, ip, session = "";
	private List<String> flags = new ArrayList<String>();
	private boolean admin = false;
	@JsonIgnore private String password = "";
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getSessionID() {
		return session;
	}
	public void setSessionID(String session) {
		this.session = session;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public List<String> getFlags() {
		return flags;
	}
	public boolean hasFlag(String flag){
		return flags.contains(flag);
	}
	public void setFlags(List<String> flags) {
		this.flags = flags;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public User(String Username, String Password, String Email, List<String> flags){
		this.setUsername(Username.toLowerCase());
		this.setPassword(Password);
		this.setEmail(Email);
		this.setFlags(flags);
	}
	public long get_id() {
		return _id;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username.toLowerCase();
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String Password) {
		String hashedPass = "";
		try {
			hashedPass = this.hashGenerator(Password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		this.password = hashedPass;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean passwordMatch(String Password){
		String hashedPass = "";
		try {
			hashedPass = this.hashGenerator(Password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if(hashedPass.equals(password)){
			return true;
		}
		return false;
	}
	public static int existsUser(String user){
		try{
			TypedQuery<User> query = ControlPanel.em.createQuery("SELECT c FROM User c WHERE c.username = :user", User.class);
			return query.setParameter("user", user.toLowerCase()).getResultList().size();
		}catch(Exception e){
			return 0;
		}
	}
	public static User findUser(String user){
		try{
			TypedQuery<User> query = ControlPanel.em.createQuery("SELECT c FROM User c WHERE c.username = :user", User.class);
			if(query!=null){
				query = query.setParameter("user", user.toLowerCase());
				if(query!=null){
					return query.getSingleResult();
				}else{
					System.out.println("oh boy... 2");
				}
			}else{
				System.out.println("oh boy...");
			}
			return null;
		}catch(Exception e){
			return null;
		}
	}
	public static User findUserSession(String uuid){
		try{
			TypedQuery<User> query = ControlPanel.em.createQuery("SELECT c FROM User c WHERE c.session = :session", User.class);
			if(query!=null){
				query = query.setParameter("session", uuid);
				if(query!=null){
					return query.getSingleResult();
				}else{
					return null;
				}
			}else{
				return null;
			}
		}catch(Exception e){
			return null;
		}
	}
	public static User findUser(long uid){
		try{
			TypedQuery<User> query = ControlPanel.em.createQuery("SELECT c FROM User c WHERE c.id = :id", User.class);
			return query.setParameter("id", uid).getSingleResult();
		}catch(Exception e){
			return null;
		}
	}
	private String hashGenerator(String Password) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(Password.getBytes());
        byte byteData[] = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}
}