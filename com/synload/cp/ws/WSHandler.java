package com.synload.cp.ws;

import java.io.IOException;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.synload.cp.ControlPanel;
import com.synload.cp.elements.JavascriptIncludes;
import com.synload.cp.handlers.PageSwitch;
import com.synload.cp.handlers.Request;
import com.synload.cp.models.User;
import com.synload.eventsystem.EventPublisher;
import com.synload.eventsystem.events.ConnectEvent;

@WebSocket
public class WSHandler{
	public Session session = null;
	public User user = null;
	public ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	/*@OnWebSocketFrame
	public void onWebSocketBinary(byte[] arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}*/
	
	@OnWebSocketClose
	public void onWebSocketClose(int statusCode, String reason) {
		ControlPanel.users.remove(session);
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@OnWebSocketConnect
	public void onWebSocketConnect(Session session) {
		this.session = session;
    	ControlPanel.users.add(session);
		try {
			session.getRemote().sendString(ow.writeValueAsString(new JavascriptIncludes()));
		} catch (IOException e) {
			e.printStackTrace();
		}
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        EventPublisher.raiseEventThread(new ConnectEvent(this));
	}
	
	@OnWebSocketError
	public void onWebSocketError(Throwable t) {
		System.out.println("Error: " + t.getMessage());
	}
	
	@OnWebSocketMessage
	public void onWebSocketText(String message){
		ObjectMapper mapper = new ObjectMapper();
		try {
			Request request = mapper.readValue(message, Request.class);
			PageSwitch.page(this,request);
		} catch (IOException e) {
			e.printStackTrace();
		}
        System.out.println("Message: " + message);
	}
}