package com.synload.cp.security;

import java.util.ArrayList;
import java.util.List;

import com.synload.cp.ControlPanel;

public class AccessViolation {
	public static void accessViolation(String ipAddress){
		long millis = System.currentTimeMillis();
		if(!ipAddress.equals("")){
			if(ControlPanel.failedAttempts.containsKey(ipAddress)){
				int counts = 0;
				List<Long> attempts = ControlPanel.failedAttempts.get(ipAddress);
				for(long attempt: attempts){
					if(millis<5000+attempt){
						counts++;
					}
				}
				if(counts>=ControlPanel.totalFailures){
					ControlPanel.bannedIPs.add(ipAddress);
				}else{
					attempts.add(millis);
				}
			}else{
				List<Long> attempts = new ArrayList<Long>();
				attempts.add(millis);
				ControlPanel.failedAttempts.put(ipAddress, attempts);
			}
		}
	}
}
