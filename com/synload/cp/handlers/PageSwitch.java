package com.synload.cp.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.synload.cp.ControlPanel;
import com.synload.cp.elements.*;
import com.synload.cp.models.User;
import com.synload.cp.pages.DashboardPage;
import com.synload.cp.pages.FullPage;
import com.synload.cp.pages.UserSettingsForm;
import com.synload.cp.users.Authentication;
import com.synload.cp.ws.WSHandler;
import com.synload.eventsystem.EventPublisher;
import com.synload.eventsystem.events.RequestEvent;

public class PageSwitch{
	public static void page(WSHandler user, Request request){
		try {
			if(user.user!=null){
				if(request.getPage().equalsIgnoreCase("dashboardItems") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new DashboardItems()));
				}else if(request.getPage().equalsIgnoreCase("full") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new FullPage()));
				}else if(request.getPage().equalsIgnoreCase("dashboardPage") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new DashboardPage()));
				}else if(request.getPage().equalsIgnoreCase("userSettings") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(UserSettingsForm.get(user)));
				}else if(request.getPage().equalsIgnoreCase("userSettings") && request.getRequest().equalsIgnoreCase("action")){
					User mu = user.getUser();
					ControlPanel.em.getTransaction().begin();
					mu.setFirstName(request.getData().get("firstname"));
					mu.setLastName(request.getData().get("lastname"));
					mu.setCity(request.getData().get("city"));
					mu.setCountry(request.getData().get("country"));
					mu.setEmail(request.getData().get("email"));
					ControlPanel.em.getTransaction().commit();
					user.session.getRemote().sendString(user.ow.writeValueAsString(new DashboardItems()));
				}else{
					EventPublisher.raiseEventThread(new RequestEvent(user,request));
				}
			}else{
				if(request.getPage().equalsIgnoreCase("login") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new LoginBox()));
				}else if(request.getPage().equalsIgnoreCase("register") && request.getRequest().equals("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new RegisterBox()));
				}else if(request.getPage().equalsIgnoreCase("full") && request.getRequest().equalsIgnoreCase("get")){
					user.session.getRemote().sendString(user.ow.writeValueAsString(new FullPage()));
				}else if(request.getPage().equalsIgnoreCase("sessionlogin") && request.getRequest().equalsIgnoreCase("get")){
					User authedUser = Authentication.session(
							user.session.getRemoteAddress().getAddress().getHostAddress(),
							request.getData().get("sessionid")
						);
					if(authedUser!=null){
						user.setUser(authedUser);
						Authenticated authResponse = new Authenticated();
						Map<String, String> userData = new HashMap<String,String>();
						userData.put("id", String.valueOf(user.getUser().get_id()));
						userData.put("session", request.getData().get("sessionid"));
						userData.put("uuid", user.session.getRemoteAddress().getAddress().getHostAddress());
						if(authedUser.getFlags()!=null){
							userData.put("flags", user.ow.writeValueAsString(authedUser.getFlags()));
						}
						userData.put("name", user.getUser().getUsername());
						authResponse.setData(userData);
						user.session.getRemote().sendString(user.ow.writeValueAsString(authResponse));
						user.session.getRemote().sendString(user.ow.writeValueAsString(new SuccessLogin()));
					}else{
						user.session.getRemote().sendString(user.ow.writeValueAsString(new FailedLogin()));
					}
				}else if(request.getRequest().equals("action") && request.getPage().equalsIgnoreCase("login")){
					User authedUser = Authentication.login(
							request.getData().get("username").toLowerCase(),
							request.getData().get("password"));
					if(authedUser!=null){
						String uuid = UUID.randomUUID().toString();
						ControlPanel.em.getTransaction().begin();
							authedUser.setIp(user.session.getRemoteAddress().getAddress().getHostAddress());
							authedUser.setSessionID(uuid);
						ControlPanel.em.getTransaction().commit();
						user.setUser(authedUser);
						Authenticated authResponse = new Authenticated();
						Map<String, String> userData = new HashMap<String,String>();
						userData.put("id", String.valueOf(user.getUser().get_id()));
						userData.put("session", uuid);
						if(authedUser.getFlags()!=null){
							userData.put("flags", user.ow.writeValueAsString(authedUser.getFlags()));
						}
						userData.put("name", user.getUser().getUsername());
						authResponse.setData(userData);
						user.session.getRemote().sendString(user.ow.writeValueAsString(authResponse));
						user.session.getRemote().sendString(user.ow.writeValueAsString(new SuccessLogin()));
					}else{
						user.session.getRemote().sendString(user.ow.writeValueAsString(new FailedLogin()));
					}
				}else if(request.getRequest().equals("action") && request.getPage().equalsIgnoreCase("register")){
					List<String> flags = new ArrayList<String>();
					flags.add("game");
					flags.add("admin");
					flags.add("account");
					boolean authedUser = Authentication.create(
							request.getData().get("username").toLowerCase(),
							request.getData().get("password"),
							request.getData().get("email"),
							flags);
					if(authedUser){
						user.session.getRemote().sendString(user.ow.writeValueAsString(new SuccessRegister()));
					}else{
						user.session.getRemote().sendString(user.ow.writeValueAsString(new FailedRegister()));
					}
				}else{
					EventPublisher.raiseEventThread(new RequestEvent(user,request));
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}