package com.synload.cp.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.regex.PatternSyntaxException;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.ContextHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.synload.cp.ControlPanel;
import com.synload.eventsystem.EventPublisher;
import com.synload.eventsystem.events.WebEvent;


public class HTTPHandler  extends ContextHandler{
	private static final MultipartConfigElement MULTI_PART_CONFIG = 
			new MultipartConfigElement(
				"./uploads/", 
				943718400, 
				948718400, 
				948718400
			);
	public HTTPHandler(){
		
	}
	private static boolean openFile(String Path,String filename,HttpServletResponse response, Request baseRequest) throws IOException{
		boolean properFile = false;
		String mime = "text/html;charset=utf-8";
        try {
        	properFile = filename.matches("(?sim)([a-z.A-Z0-9]+)");
        } catch (PatternSyntaxException ex) {
        	ex.printStackTrace();
        }
        String ext = filename.split("(?sim)\\.")[1];
		if(ext.equalsIgnoreCase("js")){
			mime = "application/javascript";
		}else if(ext.equalsIgnoreCase("css")){
			mime = "text/css";
		}
        if(properFile){
        	boolean htmlExists = ( new File(Path+"/"+filename)).exists();
        	if(htmlExists){
        		response.setContentType(mime);
        		response.setCharacterEncoding("UTF-8");
                response.setStatus(HttpServletResponse.SC_OK);
                baseRequest.setHandled(true); 
        		boolean isCached = false;
        		HashMap<String, Object> htmlf = null;
        		if(ControlPanel.htmlFiles.containsKey(Path+"/"+filename)){
        			htmlf = ControlPanel.htmlFiles.get(Path+"/"+filename);
        			isCached = htmlf.get("modified").equals(( new File(Path+"/"+filename)).lastModified());
        		}
        		if(!isCached){
	        		File htmlFile = ( new File(Path+"/"+filename));
	        		InputStream is = new FileInputStream(htmlFile);
	        		HashMap<String, Object> tmpf = new HashMap<String, Object>(); 
	        		tmpf.put("modified", ( new File(Path+"/"+filename)).lastModified());
	        		@SuppressWarnings("unused")
					int bytesRead;
	        		byte[] buffer = new byte[8 * 1024];
	        		String dataOut = "";
	        		while ((bytesRead = is.read(buffer)) != -1) {
	        			String dataM = new String(buffer);
	        			dataOut += dataM;
	        			response.getWriter().print(dataM.trim());
	        		}
	        		tmpf.put("data", dataOut);
	        		ControlPanel.htmlFiles.put(Path+"/"+filename, tmpf);
	        		is.close();
	        		return true;
        		}else{
        			response.getWriter().print(((String)htmlf.get("data")).trim());
        			return true;
        		}
        	}
        }
        return false;
	}
	@Override
	public void doHandle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException 
	{
		if(ControlPanel.bannedIPs.contains(baseRequest.getRemoteAddr())){
			response.setContentType("text/html;charset=utf-8");
    		response.setCharacterEncoding("UTF-8");
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			baseRequest.setHandled(true); 
			return;
		}
		if (request.getContentType() != null && request.getContentType().startsWith("multipart/form-data")) {
			baseRequest.setAttribute(Request.__MULTIPART_CONFIG_ELEMENT, MULTI_PART_CONFIG);
		}
		String[] URI = target.split("/");
		EventPublisher.raiseEventThread(new WebEvent(target,baseRequest,request,response,URI),false);
		if(URI.length==0){
			openFile("pages","index.html",response,baseRequest);
		}else if(URI.length>=3){
			if(!URI[1].equalsIgnoreCase("api")){
				openFile(URI[1],URI[2],response,baseRequest);
			}
		}else if(URI.length==2){
		}
	}
}