package com.synload.cp.handlers;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@JsonTypeInfo(
	use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "class"
)
public class CallEvent {
	public String event = "";
	public CallEvent setEvent(String event) {
		this.event = event;
		return this;
	}
}
