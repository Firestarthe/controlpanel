package com.synload.cp.users;

import java.util.List;
import java.util.regex.PatternSyntaxException;

import com.synload.cp.ControlPanel;
import com.synload.cp.models.User;

public class Authentication{
	public static User login(String username, String password){
		User u = null;
		if((u = User.findUser(username)) != null){
			if(u.passwordMatch(password)){
				return u;
			}
		}
		return null;
	}
	public static boolean create(String username, String password, String email, List<String> flags){
		try {
			if(!email.matches("(?i)\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b")){
				return false;
			}
		} catch (PatternSyntaxException e) {
		}
		if(User.existsUser(username)==0 && username.length()>3 && password.length()>3){
			User meow = new User( username, password, email, flags );
			ControlPanel.em.getTransaction().begin();
			ControlPanel.em.persist(meow);
			ControlPanel.em.getTransaction().commit();
			return true;
		}else{
			return false;
		}
	}
	public static User session(String ip, String uuid){
		User u = null;
		if((u = User.findUserSession(uuid)) != null){
			if(u.getIp().equals(ip)){
				return u;
			}
		}
		return null;
	}
}